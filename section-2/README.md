### External Dependency
1. yarn
1. prettier (*not required*)

### Starting the project
```
yarn start
navigate to localhost:3000
```

### Dependencies
- typescript : For typing inference and type safety
- Materiel-ui : Consistent design language
- styled-components: Big fan of css-in-js
- lodash.set : Small shortcut in transforming form inputs
- reudx-toolkit: Majority of what I will use is already preloaded in it (redux, immer)
- eslint: Code linting

### Others
- First time using typescript in a more complex project
- Lack of available time caused me to implement the bare minimum
- Faced issue with WSL2 windows server bridge
