import React, { RefObject, useRef, useState } from "react";
import AddIcon from "@material-ui/icons/Add";
import { Fab, Dialog, DialogActions, DialogTitle, DialogContent, TextField, Button, Grid } from "@material-ui/core";
import styled from "styled-components";

const ActionButton = styled(Fab)({
  position: "fixed",
  right: "5vw",
  bottom: "2.5vh",
});

interface AddUser {
  formTrapper: (formRef: RefObject<HTMLFormElement>) => void;
}

const AddUser = ({ formTrapper }: AddUser) => {
  const ref = useRef<HTMLFormElement>(null);
  const [displayed, setDisplayed] = useState(false);

  const openModal = () => {
    setDisplayed(true);
  };

  const closeModal = () => {
    setDisplayed(false);
  };

  const submitModal = () => {
    formTrapper(ref);
    setDisplayed(false);
  };

  return (
    <>
      <Dialog open={displayed} onClose={closeModal}>
        <DialogTitle id="form-dialog-title">Add a User</DialogTitle>
        <DialogContent>
          <form ref={ref}>
            <Grid container spacing={1}>
              <Grid item xs={2}>
                <TextField data-property="name.title" type="text" label="Title" fullWidth />
              </Grid>
              <Grid item xs={5}>
                <TextField data-property="name.first" type="text" label="First Name" fullWidth />
              </Grid>
              <Grid item xs={5}>
                <TextField data-property="name.last" type="text" label="Last Name" fullWidth />
              </Grid>
              <Grid item xs={12}>
                <TextField data-property="email" label="Email Address" type="email" fullWidth />
              </Grid>
              <Grid item xs={2}>
                <TextField data-property="location.street.number" type="text" label="Street #" fullWidth />
              </Grid>
              <Grid item xs={5}>
                <TextField data-property="location.street.name" type="text" label="Street Name" fullWidth />
              </Grid>
              <Grid item xs={5}>
                <TextField data-property="location.city" type="text" label="City" fullWidth />
              </Grid>
              <Grid item xs={4}>
                <TextField data-property="location.state" type="text" label="State" fullWidth />
              </Grid>
              <Grid item xs={4}>
                <TextField data-property="location.country" type="text" label="Country" fullWidth />
              </Grid>
              <Grid item xs={4}>
                <TextField data-property="location.postcode" type="number" label="Postcode" fullWidth />
              </Grid>
              <Grid item xs={6}>
                <TextField data-property="phone" type="tel" label="Phone" fullWidth />
              </Grid>
              <Grid item xs={6}>
                <TextField data-property="cell" type="tel" label="Cell" fullWidth />
              </Grid>
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeModal}>Cancel</Button>
          <Button onClick={submitModal}>Submit</Button>
        </DialogActions>
      </Dialog>
      <ActionButton aria-label="add">
        <AddIcon onClick={openModal} />
      </ActionButton>
    </>
  );
};

export default AddUser;
