import React, { useState } from "react";
import { UserData } from "../react-app-env";
import {
  Card as MuiCard,
  CardContent,
  Typography,
  CardHeader,
  Avatar,
  Grid,
  CardActions,
  Button,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

import styled from "styled-components";

interface Card {
  user: UserData;
  deleteUser: (email: string) => void;
}

const SectionTitle = styled(Typography)({
  fontSize: `0.9em`,
});

interface AlertDisplayProps {
  alertDisplayed: boolean;
}

const CardWrapper = styled(MuiCard).withConfig<AlertDisplayProps>({
  shouldForwardProp: (prop) => prop !== `alertDisplayed`,
})<AlertDisplayProps>(({ alertDisplayed }) => ({
  display: `flex`,
  flexDirection: `column`,
  justifyContent: `space-between`,
  position: `relative`,
  "> *": {
    filter: alertDisplayed ? `blur(1.5px)` : `blur(0)`,
    transition: `filter 0.7 ease-out`,
    userSelect: alertDisplayed ? `none` : `auto`,
    ":last-child": {
      filter: `none`,
      userSelect: `auto`,
    },
  },
}));

const Alert = styled.div.withConfig<AlertDisplayProps>({
  shouldForwardProp: (prop) => prop !== `alertDisplayed`,
})<AlertDisplayProps>(({ alertDisplayed }) => ({
  color: "#fdfdfd",
  position: "absolute",
  background: "#797676",
  textAlign: "center",
  display: alertDisplayed ? "flex" : "none",
  padding: "1em",
  top: "50%",
  transform: "translateY(-50%)",
  alignItems: "center",
  flexDirection: "column",
  "> *:last-child": {
    justifyContent: "center",
  },
  button: {
    color: "#fdfdfd",
  },
}));

const Card = ({ user: { name, picture, email, location, phone, cell }, deleteUser }: Card) => {
  const [isAlertDisplayed, setIsAlertDisplayed] = useState(false);

  return (
    <CardWrapper alertDisplayed={isAlertDisplayed}>
      <CardHeader
        avatar={
          <Avatar aria-label="avatar">
            <img alt="avatar" src={picture.thumbnail} />
          </Avatar>
        }
        title={`${name.title} ${name.first} ${name.last}`}
        subheader={email}
        subheaderTypographyProps={{ color: `textSecondary` }}
      />

      <CardContent>
        <SectionTitle color="textSecondary">Address</SectionTitle>
        <Typography
          gutterBottom
        >{`${location.street.number} ${location.street.name} ${location.city} ${location.state}`}</Typography>

        <Grid container spacing={2}>
          <Grid item>
            <SectionTitle color="textSecondary">Country</SectionTitle>
            <Typography gutterBottom>{`${location.country}`}</Typography>
          </Grid>

          <Grid item>
            <SectionTitle color="textSecondary">PostCode</SectionTitle>
            <Typography gutterBottom>{`${location.postcode}`}</Typography>
          </Grid>
        </Grid>

        <Grid container spacing={5}>
          <Grid item>
            <SectionTitle color="textSecondary">Phone</SectionTitle>
            <Typography gutterBottom>{`${phone}`}</Typography>
          </Grid>

          <Grid item>
            <SectionTitle color="textSecondary">Cell</SectionTitle>
            <Typography gutterBottom>{`${cell}`}</Typography>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions disableSpacing>
        <Grid container justify="flex-end">
          <Grid item>
            <Button onClick={() => setIsAlertDisplayed(true)} size="small" startIcon={<DeleteIcon />}>
              Delete
            </Button>
          </Grid>
        </Grid>
      </CardActions>
      <Alert alertDisplayed={isAlertDisplayed}>
        <Typography variant="subtitle2" gutterBottom>
          Are you sure you want to delete {email}?
        </Typography>
        <Grid container>
          <Grid item>
            <Button onClick={() => deleteUser(email)}>Yes</Button>
          </Grid>
          <Grid item>
            <Button onClick={() => setIsAlertDisplayed(false)}>No</Button>
          </Grid>
        </Grid>
      </Alert>
    </CardWrapper>
  );
};
export default Card;
