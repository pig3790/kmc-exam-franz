import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { store } from "./app/Store";
import { Provider } from "react-redux";
import { CssBaseline, ThemeProvider, StylesProvider } from "@material-ui/core";

import theme from "./theme";
import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle(() => ({
  "#root": {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

ReactDOM.render(
  <React.StrictMode>
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <CssBaseline />
          <GlobalStyles />
          <App />
        </Provider>
      </ThemeProvider>
    </StylesProvider>
  </React.StrictMode>,
  document.getElementById("root"),
);
