/// <reference types="react-scripts" />

export interface UserData {
  gender: string;
  name: { title: string; first: string; last: string };
  location: {
    street: { number: number; name: string };
    city: string;
    state: string;
    country: string;
    postcode: number;
    coordinates: { latitude: string };
    timezone: { offset: string };
  };
  email: string;
  login: {
    uuid: string;
    username: string;
    password: string;
    salt: string;
    md5: string;
    sha1: string;
    sha256: string;
  };
  dob: { date: string; age: number };
  registered: { date: string; age: number };
  phone: string;
  cell: string;
  id: { name: string };
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
  nat: string;
}

export interface RandomUserResult {
  results: UserData[];
  info: {
    seed: string;
    results: number;
    page: number;
    version: string;
  };
}

import { Theme } from "@material-ui/core/styles/createMuiTheme";

declare module "@material-ui/core" {
  export interface BaseTheme {
    theme: Theme;
  }
}
