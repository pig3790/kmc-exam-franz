import { RandomUserResult, UserData } from "../../react-app-env";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "./index";

interface initialState {
  status: `pending` | `idle`;
  list: UserData[];
}

const initialState: initialState = {
  status: "idle",
  list: [],
};

export const preloadUsers = createAsyncThunk(
  `users/preloadUsers`,
  async (_skip, { dispatch }) => {
    dispatch(setPending());
    const data = (await (await fetch(`https://randomuser.me/api/?results=100`)).json()) as RandomUserResult;
    dispatch(setIdle());
    return data.results;
  },
  {
    condition: (_skip, { getState }) => {
      const { users } = getState() as RootState;
      return !users.list.length && users.status === `idle`;
    },
  },
);

export const Users = createSlice({
  name: "users",
  initialState,
  reducers: {
    setPending: (state) => {
      state.status = `pending`;
    },
    setIdle: (state) => {
      state.status = `idle`;
    },
    deleteUser: (state, { payload }) => {
      for (let i = 0; i < state.list.length; i++) {
        if (state.list[i].email === payload) {
          state.list.splice(i, 1);
          break;
        }
      }
    },
    addUser: (state, { payload }) => {
      state.list.push(payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(preloadUsers.fulfilled, (state, { payload }) => {
      payload.forEach((userData: UserData) => {
        state.list.push(userData);
      });
    });
  },
});

const { setIdle, setPending } = Users.actions;
export const { deleteUser, addUser } = Users.actions;
export const selectUsers = ({ users }: RootState): UserData[] => users.list;

export default Users.reducer;
