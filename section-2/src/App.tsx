import React, { RefObject } from "react";
import { BaseTheme, withTheme } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { addUser, deleteUser, preloadUsers, selectUsers } from "./app/Store/Users";
import styled from "styled-components";
import Card from "./Components/Card";
import AddUser from "./Components/AddUser";
import set from "lodash.set";
import { UserData } from "./react-app-env";

const UserGrid = withTheme(
  styled.div(({ theme }: BaseTheme) => ({
    display: "grid",
    gridTemplateColumns: "1fr",
    [theme.breakpoints.up(`sm`)]: {
      gridTemplateColumns: "repeat(2, 1fr)",
    },
    [theme.breakpoints.up(`md`)]: {
      gridTemplateColumns: "repeat(3, 1fr)",
    },
    [theme.breakpoints.up(`lg`)]: {
      gridTemplateColumns: "repeat(5, 1fr)",
    },
    gridGap: `1em`,
  })),
);

const AppContainer = withTheme(
  styled.main(({ theme }: BaseTheme) => ({
    width: "100%",
    padding: "5em 0",
    [theme.breakpoints.up(`md`)]: {
      maxWidth: "90vw",
    },
  })),
);

const App: React.FC = () => {
  const dispatch = useDispatch();
  const users = useSelector(selectUsers);
  dispatch(preloadUsers());

  const deleteUserAction = (email: string): void => {
    dispatch(deleteUser(email));
  };

  const addNewUser = (form: RefObject<HTMLFormElement>): void => {
    console.log(form);
    if (form && form.current) {
      interface Acc {
        picture: { thumbnail: string };
      }
      const User = Array.from(form.current)
        .map((e) => [
          (e as HTMLInputElement)?.parentElement?.parentElement?.dataset.property ?? false,
          (e as HTMLInputElement).value,
        ])
        .reduce(
          (acc, [key, value]): UserData | Acc => {
            type set = (acc: Record<string, unknown>, key: string, value: string | boolean) => void;
            if (typeof key === "string") (set as set)(acc, key, value);
            return acc;
          },
          {
            picture: {
              thumbnail: `https://randomuser.me/api/portraits/thumb/women/${Math.floor(Math.random() * 99) + 1}.jpg`,
            },
          },
        );

      dispatch(addUser(User));
    }
  };

  return (
    <AppContainer>
      <UserGrid>
        {users.map((user) => (
          <Card key={user.email} user={user} deleteUser={deleteUserAction} />
        ))}
      </UserGrid>
      <AddUser formTrapper={addNewUser} />
    </AppContainer>
  );
};

export default App;
