> How long did you spend on this assessment?

A small amount of time for the first question, and about one and a half day for the second.

> What else would you add to your solution if you had more time?
- Edit user
- More info for the user card
- A way to pick an avatar
- A user dedicated page

> List a few of your preferred JS/React frameworks (also let us know when you would choose to use/not use them)

| |Use | Won't Use |
|---	|---	|---	|
| Ramada |  When problem requires a more functional approach|  Teammates are not familiar with functional programming|
| Reselect | When app requires a trackable caching layer| Small applications|
| Date-Fns | When working with lots of dates | Small amount of date transforms |

> When a React component needs to make an asynchronous call (such as to an API) what is your preferred approach to achieve this?

I prefer doing all the async function calls inside a side-effect in the state manager. This avoids an issue that pops up when doing it inside a react component.(ex: when an async call updates a state and the component has already unmounted causing a memory leak.)

> Have you had any experience with frontend E2E testing frameworks like cypress.io? (If so please tell us about your experience).

I have little experience with it, unfortunately all the teams that I've joined have already their own qa pipelines(mostly with cucumber.)

> What do you think is the most interesting trend in React development or frontend development in general?

It brings a lot of innovations to the frontend space as well as the multitude of ways to work with it, One good example is the stuff they've released with suspense. May not be strictly frontend but I like javascript in general, because it gives me a way to work in multiple environments even when they were not originally envisioned to run it (e.g. Frontend, Backend, IOT, App Development, AI, Blockchain). Plus Dan Abramov.

