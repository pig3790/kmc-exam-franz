function getLessThan(list, index) {
  if (list[list.length - 1] < index) return list.length
  let count = 0
  let left = list.slice()
  let right = left.splice(Math.floor(left.length / 2))

  while (true) {
    if (right[0] < index) {
      count += left.length
      left = right.slice()
      right = left.splice(Math.floor(left.length / 2))
      continue
    }
    break
  }
  for (const num of left) {
    if (num < index) count++
    if (num >= index) break
  }
  return count
}
